from sanic import Sanic

from controller.c import process_handler

app = Sanic(__name__)

app.add_route(process_handler, "/process", methods=["GET"])

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8000, debug=True)
