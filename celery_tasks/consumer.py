from celery import Celery

celery_consumer = Celery("celery_consumer")
celery_consumer.config_from_object("celery_tasks.config")


celery_consumer.autodiscover_tasks(
    [
        "celery_tasks.alarm",
    ]
)
