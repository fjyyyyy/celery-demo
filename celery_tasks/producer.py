from celery import Celery

celery_producer = Celery("celery_producer")
celery_producer.config_from_object("celery_tasks.config")
