from celery_tasks.consumer import celery_consumer


@celery_consumer.task
def add(x, y):
    return x + y
