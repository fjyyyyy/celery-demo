import asyncio

from celery_tasks.producer import celery_producer


class Service:
    async def process(self, p1, p2):
        print("processing")
        res = celery_producer.send_task("celery_tasks.alarm.tasks.add", args=[p1, p2])
        while not res.ready():
            await asyncio.sleep(1)
        print("done")

        return res.get()
