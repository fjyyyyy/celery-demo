from sanic import Request, json

from service.s import Service

svc = Service()


async def process_handler(request: Request):
    p1, p2 = int(request.args.get("p1")), int(request.args.get("p2"))
    res = await svc.process(p1, p2)
    return json({"message": res})
